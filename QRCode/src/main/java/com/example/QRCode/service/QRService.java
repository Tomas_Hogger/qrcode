package com.example.QRCode.service;

import com.example.QRCode.Helper.QRCodeWriter;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import org.springframework.stereotype.Service;

import javax.xml.bind.DatatypeConverter;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

@Service
public class QRService {

    private static final String KEY = "db7827cc775511e98f9e2a86e4085a59";

    public List<BitMatrix> getBitMatrixList(int id, String[] numbers) throws WriterException, NoSuchAlgorithmException {
        List<BitMatrix> bitMatrices = new ArrayList<>();
        for(String s : numbers){
            QRCodeWriter qrCodeWriter = new QRCodeWriter();
            bitMatrices.add(qrCodeWriter.encode(id + " " + s + " " + getSSH1(id,s), BarcodeFormat.QR_CODE, 450, 450));
        }
        return bitMatrices;
    }


    public boolean check_ssh(int id, String number, String ssh1) throws NoSuchAlgorithmException {
        return getSSH1(id,number).equals(ssh1);
    }

    private String getSSH1(int id, String number) throws NoSuchAlgorithmException {
        MessageDigest msg = MessageDigest.getInstance("SHA-1");
        String str = id+number+KEY;
        msg.update(str.getBytes(StandardCharsets.UTF_8), 0, str.length());
        return DatatypeConverter.printHexBinary(msg.digest());
    }
}
